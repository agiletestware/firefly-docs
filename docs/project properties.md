List of project properties and their functions

---
!!! tip "TestSuite properties"
    Some properties like ==TR_run_name==, ==TR_plan_name== can also be configured at the testsuite level. If specified at the testsuite level, the testsuite value will take precedence over project values.

!!! note
    Specifying these values at the testsuite is very useful when users want to map each testsuite to a different Section/Sub-Section, Test Plan, TestRun.


| Property | Purpose  					| Example				 | Required |
|----------|----------------------------|------------------------|----------|
| TR_url   | TestRail server URL        | https://test.testrail.net  	 | Yes		|
| TR_project| Name of a TestRail project| project| Yes
| TR_user| TestRail user name| username@company.com| Yes
| TR_password| TestRail user password or API key| API_KEY | Yes
| TR_section_path | Path to TestRail section\sub-section where Firefly will export tests | Master\Section 1\Section 2 | Yes
| TR_firefly_plugin | Enables or disables Firefly. Valid values: ==enabled== or ==disabled==| enabled | Yes
| TR_run_name | Name of a run in TestRail to which Firefly will export test results for all tests in the project (can be specified for individual test suites). If run does not exist, it will be created| Sample Run | No
| TR_plan_name | Name of a test plan in TestRail to which Firefly will export test results for all tests in the project (can be specified for individual test suites). If plan does not exist, it will be created| Sample Plan | No
| TR_milestone | Name of a milestone or path (with \ as delimiter) to a nested milestone to which Test Run or Test Plan shall be mapped | milestone1 or milestone1\milestone2 | No
| TR_case_type | Name of a case type in TestRail. If it is not set, default case type will be used. Available case types can be viewed on ==Administration -> Customization -> Case Types== screen on TestRail server| Automated | No
| TR_template_name| Name of a TestRail template to use for cases. If it is not set, "Test Case (Steps)" template name will be used. Available templates can be viewed on ==Administration -> Customization -> Templates== | Test Case (Steps) | No
| TR_upload_attachments | If ==enabled==, Firefly captures HTTP/SOAP request and response and attaches it to a result in TestRail. Valid values: ==enabled== and ==disabled== | enabled | No
| TR_id | Firefly internal field. Should not be edited by user. | N/A | N/A

Example: Project level custom properties

[![Project Custom Properties](images/project-custom-props.png "Project Custom Properties")](images/project-custom-props.png "Project Custom Properties")
