title: Test Suite Properties - Firefly Docs
description: List of suite properties and their functions

# Test Suite Properties
List of suite properties and their functions

| Property | Purpose  					| Example				 | Required |
|----------|----------------------------|------------------------|----------|
| TR_section_path | Path to TestRail suite\section where Firefly will export tests for this particular test suite. It takes precedence over value on Project level | Master\Section 1\Section 2 | Yes
| TR_run_name | Name of a run in TestRail to which Firefly will export test results for all tests in the test suite. If run does not exist, it will be created| Sample Run | No
| TR_plan_name | Name of a test plan in TestRail to which Firefly will export test results for all tests in the test suite. If plan does not exist, it will be created| Sample Plan | No
| TR_case_type | Name of a case type in TestRail. Overrides value on project level. Available case types can be viewed on ==Administration -> Customization -> Case Types== screen on TestRail server | Automated | No
| TR_id | Firefly internal field. Should not be edited by user. | N/A | N/A

Example: Test Suite level custom properties

[![Suite Custom Properties](images/suite-custom-props.png "Project Custom Properties")](images/suite-custom-props.png "Suite Custom Properties")
