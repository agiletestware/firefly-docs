title: FireFly Quick Start Guide - Firefly Docs
description: Follow these steps to get up and running in 10 minutes.

# Quick start
Follow these steps to get up and running in 10 minutes.

------------------------------------------------------------


**Step 1:**	Start ReadyAPI and select a project which needs to be exported to TestRail.

**Step 2:**	Right click on a project and choose ==Firefly: (Un)initialize project==. This will add all required custom properties to your project.  

![""](images/initialize-project-1.png "Initialize project")

!!! note "Removing Firefly Custom Properties"
    Initializing the project injects new properties into your project. If you don't need these properties anymore, simply right click on project and select ==Firefly: (Un)initialize project==

**Step 3:** Configure the ReadyAPI project's ==custom properties==. The high lighted properties must be configured to reflect your TestRail environment. A detailed explanation of all the [Firefly Project Properties](project properties.md) can be found in the docs.

![""](images/project-properties.png "Project properties")

**Step 4:**	Export ReadyAPI project into TestRail. Simply right click on ==Firefly: Export to TestRail==.
Firefly plugin will show a progress bar and live export status in the Firefly log tab.

![""](images/export-project-1.png "Exporting project")  


![""](images/export-project-2.png "Exporting project")

!!! note "Project, TestSuite, or TestCase level export"
    Export to TestRail action can also be invoked from the ReadyAPI testsuite or testcase. Project action will export/update the entire project. Whereas, testsuite or testcase level actions will only export/update the specified testsuite and testcase respectively. This is useful when you may have only changed one testcase or testsuite and don't need to upload the entire project which will be slower.

**Step 5:**	The ReadyAPI tests should now be visible under ==TestRail > TestCases > Section==

![""](images/export-project-3.png "Exporting project")

![""](images/export-test-steps-details.png "test step details")


**Step 6:**	Run tests from ReadyAPI

!!! success "Running ReadyAPI tests from CI Systems"
    Most organizations use CI applications like Jenkins, Bamboo, TeamCity, CircleCI, etc. If you run your ReadyAPI tests from these CI application, Firefly will automatically detect and export ReadyAPI tests results. There is no additional configuration needed.

![""](images/export-results-1.png "Exporting test results")

**Step 7:**	You can now view the ReadyAPI tests and test results in TestRail.

![""](images/export-results-2.png "Exporting test results")

![""](images/export-results-3.png "Exporting test results")


!!! success "Mapping Test Results to TestRail Test Runs, Test Plans, and/or Milestones"
    Every customer has unique workflows. Firefly's flexible configurations allows users to map their test results to TestRail Test Runs, Test Plans, and/or Milestones in a variety of ways. This section describes some of the ways you can configure test results mapping.

## Updating test cases in TestRail with the latest changes from ReadyAPI
Whenever a change is done in ReadyAPI for a test case (e.g. number of steps, step names, custom fields are changed, etc), this test case have to be re-exported into TestRail so changes are reflected in TestRail. To do that, just right click on a test case, test suite or project and click: ==Firefly: Export to TestRail==.

## Map  Results to Test Runs

In cases where you need to store all test results in a ==single Test Run==, you can set the ==TR_run_name== field on either ReadyAPI Project or TestSuite level.

![""](images/run-name-1.png "Setting run name")

If Firely is able to locate the specified Test Run in TestRail, Firefly will add ReadyAPI test results to this Test Run. If the specified ==TR_run_name== value does not exist, Firefly will create one automatically. If the ==TR_run_name== value is empty, Firefly will automatically create a run using ==Run: <date> <HH:MM:SS>== naming format.


**Example:** ReadyAPI project configured with ==TR_run_name:Test Run==. All subsequent runs in ReadyAPI will export the test results to the same Test Run in TestRail.
![""](images/run-name-2.png "Setting run name")

## Map  Results to Test Plans

In TestRail, multiple Test Runs are best organized under Test Plans. Firefly allows users to specify both Test Run and Test Plan values using the ==TR_plan_name== custom property. ==TR_plan_name== can be specified at the project or testsuite level.

![""](images/plan-name-1.png "Setting plan name")

If Firely is able to locate the specified Test Plan, it will add the Test Run to this Test Plan. If the Test Plan value does not exist, Firefly will create one automatically.


![""](images/plan-name-2.png "Setting plan name")

## Map Test Run/Plan to Milestone

Test Runs and Test Plans in TestRail are typically assigned to some Milestone. To assign ReadyAPI tests to some Milestone, Firefly provides the ==TR_milestone== custom property. If the Milestone value does not exist, Firefly will create one automatically.
**Example:** ==_milestone1_== or path to a nested milestone: ==_milestone1\milestone2_==

 ![""](images/milestones-1.png "Milestones")


 ![""](images/milestones-2.png "Milestones")
