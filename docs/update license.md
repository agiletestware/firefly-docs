title: Updating Your License - Firefly Documentation
description: How to update Firefly license

# Updating Firefly license file

To update Firefly license file, replace the existing license file located in ==%USERPROFILE%\firefly== folder. On Windows systems, this is usually ==C:\Users\\_user_\\firefly==.
