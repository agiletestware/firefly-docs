title: Firefly Custom Properties Guide - Firefly Docs
description: Firefly allows users to easily configure custom fields by using a configuration wizard.

# Custom Properties
In many TestRail installations, users define custom user fields for test cases. For example:

![""](images/custom-fields-1.png "Custom properties")

Firefly allows users to easily configure custom fields by using a configuration wizard.
To open the wizard, simple right click on your project and select ==Firefly: Configure custom fields==.

![""](images/custom-fields-2.png "Custom properties")

![""](images/custom-fields-3.png "Custom properties")  

Click on ==Add button (green plus icon)== and specify TestRail field label, readyAPI field name and default value.

![""](images/custom-fields-4.png "Custom properties")

Click ==**Save**==. The default values for each of the custom properties will be added to all Test Cases in ReadyAPI project.

![""](images/custom-fields-5.png "Custom properties")

Re-export project to TestRail. You will see that values from custom fields are transferred into TestRail Test Cases.

![""](images/custom-fields-6.png "Custom properties")

!!! tip "Using ReadyAPI property expansion"
	Firefly supports [ReadyAPI property expansion](https://support.smartbear.com/readyapi/docs/testing/properties/expansion.html) mechanism for custom fields, so you can use it in a wizard, e.g. ${#Project#Environment} which means that the value of Environment custom property of a project will be set to test case in TestRail. See the example below:

Define a new mapping entry in ReadyAPI:

![""](images/property-expansion-1.png "Property expansion")

Add an "Environment" custom property to a project:

![""](images/property-expansion-2.png "Property expansion")

And exporting results to TestRail:

![""](images/property-expansion-3.png "Property expansion")