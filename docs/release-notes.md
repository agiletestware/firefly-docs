title: Release Notes - Firefly Documentation
description: Firefly Release Notes, bugs and fixes


Release Notes
=============
**Firefly version: 2.3**

New Features:

	[FIR-82] - Add support for new TestRail bulk API response	
	
**Firefly version: 2.2**

New Features:

	[FIR-39] - MacOS installer	
	[FIR-76] - Upload request and response as attachments into TestRail
	[FIR-78] - Add support for Security tests
	[FIR-79] - Add support for Load tests

Bugs:

	[FIR-77] - Upload results fail if plan name overridden on suite level
	
**Firefly version: 2.1.0**

Bugs:

	[FIR-71] - If required field is inactive, Firefly still complains about it
	[FIR-72] - Firefly complains about required field when it is not a part of a current template
	[FIR-73] - Firefly does not set default values for required fields
	[FIR-74] - Mappings for dropdowns: firefly should match text value to the id and send id
	
**Firefly version: 2.0.9**

New Features:

	[FIR-68] - Add TR_template_name field, so template name can be configured

Bugs:

	[FIR-66] - NPE during run from Bamboo
	
**Firefly version: 2.0.8**
	
Bugs:

	[FIR-63] - java.lang.RuntimeException: Could not resolve the node to a handle at org.apache.xml.dtm.ref.DTMManagerDefault.getDTMHandleFromNode(DTMManagerDefault.java:576) when configuration dialog is being opened
	[FIR-64] - Firefly plugin does not work with ReadyAPI 2.5.0

**Firefly version: 2.0.7**

Bugs:

	[FIR-61] - NoClassDefFoundException in ReadyAPI 2.4.0
	
**Firefly version: 2.0.6**

New Features:

	[FIR-57] - Improve "(Un)Initialize action": add prompts and populate\erase related test case properties
	[FIR-58] - Add endpoint information to "content" field in a test step
	[FIR-59] - Support for ReadyAPI property expansion mechanism

Bugs:

	[FIR-56] - Custom Properties wizard: soapui_name value is null or empty in custom property configuration

**Firefly version: 2.0.5**

New Features:

	[FIR-45] - Add TR_case_type configuration field
	[FIR-50] - Improve error message for missing TR_section_path value
	[FIR-51] - Adjust default values for TR_ properties and reorder them
	[FIR-52] - Do not clear TR_suite_id during cloning of suite
	[FIR-53] - Remove TR_suite_id property from TestSuite
	[FIR-54] - Clear TR_id property when TR_section_path changes on suite level

Bugs:

	[FIR-46] - SUITE must have an id error during export results on multi-suite projects
	[FIR-47] - Incorrect logging during exporting cases to multi-suite project
	[FIR-48] - Uninitialize action does not clear TR_Id and TR_suite_id properties
	
**Firefly version: 2.0.4**

New Features:

	[FIR-41] - Remove trailing slash in TR_URL automatically to prevent errors
	[FIR-42] - Do not inject properties to projects automatically, add "Firefly: (Un)Initialize" menu action

Bugs:

	[FIR-43] - HTTP 404 for custom TestRail server URL which does not have default URL (e.g. https://<server>/testrail)
	[FIR-44] - Password is not encrypted on newly added project